import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { filter, map, multicast, publish, publishReplay, refCount, share, shareReplay, tap } from 'rxjs/operators';
import { Album } from 'src/app/core/model/album';
import { MusicApiService } from 'src/app/core/services/music-api.service';
import { MusicSearchService } from 'src/app/core/services/music-search.service';


@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.container.html',
  styleUrls: ['./album-search.container.scss']
})
export class AlbumSearchContainer implements OnInit {

  message = this.service.errorChange.pipe(map(e => e.message))
  query = this.service.queryChange
  results = this.service.albumsChange

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService,
  ) { }

  ngOnInit(): void {

    this.route.queryParamMap.pipe(
      map(params => params.get('q') || ''),
      filter((q) => q !== '')
    ).subscribe(q => {
      this.service.searchAlbums(q)
    })

  }

  searchAlbums(query: string) {
    this.router.navigate([/* '/music', 'search' */ /* '../search' */ '.'], {
      queryParams: { q: query },
      relativeTo: this.route,
      replaceUrl: true
    })
  }

}
