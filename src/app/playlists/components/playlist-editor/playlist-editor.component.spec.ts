import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, flushMicrotasks, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormsModule, NgModel } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CardComponent } from 'src/app/shared/components/card/card.component';
import { CensorDirective } from 'src/app/shared/validators/censor.directive';

import { PlaylistEditorComponent } from './playlist-editor.component';

describe('PlaylistEditorComponent', () => {
  let component: PlaylistEditorComponent;
  let fixture: ComponentFixture<PlaylistEditorComponent>;
  let elem: DebugElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistEditorComponent, CardComponent, CensorDirective],
      imports: [FormsModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistEditorComponent);
    component = fixture.componentInstance;
    elem = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should count name length', async () => {
    await fixture.whenStable()

    const fields = elem.queryAll(By.directive(NgModel))
    const name = fields.find(f => f.attributes['name'] === 'name')!

    name.nativeElement.value = 'Placki'
    name.triggerEventHandler('input', { target: name.nativeElement })
    // name.nativeElement.dispatchEvent(new InputEvent('input'))

    fixture.detectChanges();
    const counter = elem.query(By.css('[data-testid="counter"]'))
    expect(counter.nativeElement.textContent).toMatch('6 / 170')
  })

  // it('should count name length 2', fakeAsync(() => {
  // it('should count name length 2', waitForAsync(() => {
     
  //   const fields = elem.queryAll(By.directive(NgModel))
  //   const name = fields.find(f => f.attributes['name'] === 'name')!
    
  //   name.nativeElement.value = 'Placki'
  //   name.triggerEventHandler('input', { target: name.nativeElement })
  //   // name.nativeElement.dispatchEvent(new InputEvent('input'))
  //   // fixture.detectChanges();
  //   // tick()

  //   fixture.detectChanges();
  //   const counter = elem.query(By.css('[data-testid="counter"]'))
  //   expect(counter.nativeElement.textContent).toMatch('6 / 170')
  // }))
});
