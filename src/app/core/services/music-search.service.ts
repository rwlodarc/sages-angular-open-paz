import { Inject, Injectable } from '@angular/core';
import { AsyncSubject, BehaviorSubject, EMPTY, ReplaySubject, Subject } from 'rxjs';
import { catchError, concatAll, concatMap, exhaust, exhaustMap, map, mergeAll, mergeMap, switchAll, switchMap } from 'rxjs/operators';
import { Album } from '../model/album';
import { MusicApiService } from './music-api.service';
import { INITIAL_ALBUMS_SEARCH } from './tokens';

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService {

  constructor(
    @Inject(INITIAL_ALBUMS_SEARCH) private initial_results: Album[],
    private api: MusicApiService) {

    (window as any).subject = this.albums;

    this.queries.pipe(
      switchMap(query => this.api.searchAlbums(query).pipe(
        catchError(error => {
          this.errors.next(error);
          return EMPTY
        })
      )),
    ).subscribe(this.albums)
  }

  private errors = new Subject<Error>()
  private queries = new ReplaySubject<string>(5)
  private albums = new BehaviorSubject<Album[]>(this.initial_results)

  /* Outputs - Queries */
  queryChange = this.queries.asObservable()
  albumsChange = this.albums.asObservable()
  errorChange = this.errors.asObservable()


  /* Inputs - Commands */
  searchAlbums(query: string) {
    this.queries.next(query)
  }
}
