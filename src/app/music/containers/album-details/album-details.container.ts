import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY } from 'rxjs';
import { catchError, filter, finalize, map, switchMap, tap } from 'rxjs/operators';
import { MusicApiService } from 'src/app/core/services/music-api.service';


/* 
== TODO:

- show ID from url 
- http://localhost:4200/music/albums?id=5Tby0U5VndHW0SomYO7Id7
- http://localhost:4200/music/albums/5Tby0U5VndHW0SomYO7Id7

- fetch album by id using MusicApiService
- render album details
- render album-card component with album data

+ router link from search to here (with album id)

*/

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.container.html',
  styleUrls: ['./album-details.container.scss']
})
export class AlbumDetailsContainer implements OnInit {

  id = this.route.paramMap.pipe(
    map(pm => pm.get('albumId')),
  )

  album = this.id.pipe(
    switchMap(id => {
      setTimeout(() => this.loading = true)
      return this.api.getAlbumById(id!).pipe(
        catchError(error => {
          this.message = error.message; return EMPTY
        }),
        finalize(() => this.loading = false),
      )
    }),
  )

  message = ''
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private api: MusicApiService
  ) { }

  ngOnInit(): void {
  }

}
