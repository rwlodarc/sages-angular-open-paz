import { Directive } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownTriggerDirective } from './dropdown-trigger.directive';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  public trigger?: DropdownTriggerDirective
  public menu?: DropdownMenuDirective

  constructor() { 
    console.log('hello dropdown');    
  }

}
