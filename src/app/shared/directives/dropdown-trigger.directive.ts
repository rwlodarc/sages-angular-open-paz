import { Directive, ElementRef, HostBinding, HostListener, Input, Optional } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[appDropdownTrigger]'
})
export class DropdownTriggerDirective {

  @Input('appDropdownTrigger') menu: DropdownMenuDirective | string = ''

  @HostBinding('attr.appDropdownTrigger') fakeattribute = true

  @HostListener('click')
  handleClick() {
    this.getMenu().show()
    // this.menu.show()
  }

  @HostListener('document:click', ['$event.target'])
  handleClickOutside(target: HTMLElement) {
    if (target.closest('[appDropdownTrigger]') ||
      target.closest('[appDropdownMenu]')) {
      return
    }
    this.getMenu().open = false
  }

  @HostListener('document:keyup', ['$event.key'])
  handleEscape(key: string) {
    if (key === 'Escape') {
      this.getMenu().open = false
    }
  }

  getMenu() {
    if (this.menu instanceof DropdownMenuDirective) {
      return this.menu
    } else {
      return this.parent.menu!
    }
  }

  constructor(@Optional() private parent: DropdownDirective) {
    if (parent !== null) {
      this.parent.trigger = this
    }
  }

}
