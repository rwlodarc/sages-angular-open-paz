import { Directive, ElementRef, HostBinding } from '@angular/core';
import { DropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[appDropdownMenu]',
  exportAs: 'dropdownMenu'
})
export class DropdownMenuDirective {

  @HostBinding('class.show')
  @HostBinding('attr.aria-expanded')
  open = false

  constructor(private parent: DropdownDirective) { 
    this.parent.menu = this
  }

  ngOnInit() { }

  show() {
    this.open = !this.open
  }
}
