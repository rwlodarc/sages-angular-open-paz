import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Playlist } from '../../model/Playlist';

NgForOfContext
NgForOf

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsListComponent implements OnInit {

  @Input('items') listItems: Playlist[] = []

  @Input() selectedId?: Playlist['id'] = ''

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>()
  @Output() delete = new EventEmitter<Playlist['id']>()

  select(playlistId: Playlist['id']) {
    this.selectedIdChange.emit(playlistId)
  }

  remove(playlistId: Playlist['id']) {
    this.delete.emit(playlistId)
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    console.log('changed list');
    
  }

  constructor() { }

  ngOnInit(): void {
  }

}
