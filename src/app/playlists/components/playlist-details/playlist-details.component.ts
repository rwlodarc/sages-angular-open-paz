import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Track } from 'src/app/core/model/album';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  // ng-output
  @Output() edit = new EventEmitter();

  tracks: Track[] = []

  ngOnChanges(changes: SimpleChanges): void {
    if (this.playlist.tracks)
      this.tracks = this.playlist.tracks.items.map(i => i.track)
  }

  editClick() {
    this.edit.emit()
  }

  constructor() {
    // this.playlist &&  this.playlist.name
    // this.playlist?.name

    // this.playlist!.name
    // expr ? 'true' : 'false'

  }

  ngOnInit(): void {
  }

}
