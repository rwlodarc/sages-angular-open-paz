import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Track } from 'src/app/core/model/album';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.scss']
})
export class TrackListComponent implements OnInit {

  @Input() tracks: Track[] = [];

  playing?: Track

  @ViewChild('playerRef', {})
  playerRef?: ElementRef<HTMLAudioElement>

  constructor() { }

  play(track: Track) {
    this.playing = track == this.playing ? undefined : track;
    setTimeout(() => {
      if (this.playerRef) {
        // this.playerRef.nativeElement.src = track.preview_url
        this.playerRef.nativeElement.paused ?
          this.playerRef.nativeElement.play() :
          this.playerRef.nativeElement.pause()
      }
    })
  }

  ngAfterViewInit(): void {
    if (this.playerRef) {
      this.playerRef.nativeElement.volume = 0.2;
    }
  }

  ngOnInit(): void {
    // this.playing = this.tracks[3]
  }

}
