import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutedPlaylistDetailsComponent } from './components/routed-playlist-details/routed-playlist-details.component';
import { RoutedPlaylistEditorComponent } from './components/routed-playlist-editor/routed-playlist-editor.component';
import { PlaylistsNestedViewComponent } from './containers/playlists-nested-view/playlists-nested-view.component';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistResolver } from './resolvers/playlist.resolver';
import { PlaylistsResolver } from './resolvers/playlists.resolver';

const routes: Routes = [
  // {
  //   path: 'oldplaylists',
  //   component: PlaylistsViewComponent
  // },
  {
    path: 'playlists',
    data: {
      placki: 123
    },
    resolve: {
      playlists: PlaylistsResolver
    },
    component: PlaylistsNestedViewComponent,
    children: [
      {
        path: ':id',
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'details'
          },
          {
            resolve: {
              playlist: PlaylistResolver
            },
            path: 'details',
            component: RoutedPlaylistDetailsComponent
          },
          {
            path: 'edit',
            resolve: {
              playlist: PlaylistResolver
            },
            component: RoutedPlaylistEditorComponent
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
