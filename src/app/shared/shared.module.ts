import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YesnoPipe } from './pipes/yesno.pipe';
import { CensorDirective } from './validators/censor.directive';
import { DropdownDirective } from './directives/dropdown.directive';
import { DropdownTriggerDirective } from './directives/dropdown-trigger.directive';
import { DropdownMenuDirective } from './directives/dropdown-menu.directive';
import { CardComponent } from './components/card/card.component';
import { ClockComponent } from './clock/clock.component';
import { RecentSearchesComponent } from './containers/recent-searches/recent-searches.component';
import { DurationPipe } from './pipes/duration.pipe';
import { TrackListComponent } from './components/track-list/track-list.component';


@NgModule({
  declarations: [
    //  ng g p shared/pipes/yesno --export 
    YesnoPipe,
    ClockComponent,
    CensorDirective,
    DropdownDirective,
    DropdownTriggerDirective,
    DropdownMenuDirective,
    CardComponent,
    RecentSearchesComponent,
    DurationPipe, 
    TrackListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    YesnoPipe,
    ClockComponent,
    CensorDirective,
    DropdownDirective,
    DropdownTriggerDirective,
    DropdownMenuDirective,
    CardComponent,
    RecentSearchesComponent,
    DurationPipe, 
    TrackListComponent,
  ]
})
export class SharedModule { }
