import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamplesRoutingModule } from './examples-routing.module';
import { DropdownsComponent } from './dropdowns/dropdowns.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    DropdownsComponent
  ],
  imports: [
    CommonModule,
    ExamplesRoutingModule,
    SharedModule
  ]
})
export class ExamplesModule { }
