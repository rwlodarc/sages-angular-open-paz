import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, AbstractControl, ValidationErrors, FormBuilder, ValidatorFn, AsyncValidatorFn } from '@angular/forms';
import { combineLatest, concat, from, merge, Observable, of, Subscriber, zip } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() query: string | null = ''

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    (this.queryForm.get('query') as FormControl).setValue(this.query, {
      // onlySelf: true,
      emitEvent: false,
    })
  }

  // censor = (badword: string): ValidatorFn => (control: AbstractControl): ValidationErrors | null => {

  //   const hasBadWord = String(control.value).includes(badword)

  //   return hasBadWord ? {
  //     'censor': { badword: badword }
  //   } : null
  // }

  asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
    // from(Promise.resolve())
    return new Observable((subscriber: Subscriber<ValidationErrors | null>) => {
      const badword = 'batman'
      const hasBadWord = String(control.value).includes(badword)

      // console.log('Validation start');
      const handle = setTimeout(() => {
        const result = hasBadWord ? { 'censor': { badword: badword } } : null;

        subscriber.next(result)
        subscriber.complete()
        // console.log('Validation end');
      }, 200)

      return () => {
        // console.log('Validation cancelled');
        clearTimeout(handle)
      }
    })
    // .pipe(...)
    // .subscribe({
    //    next: result => console.log(result)
    // })
  }

  queryForm = this.fb.group({
    // 'x':new FormControl('',[],),
    'query': ['', [
      Validators.required,
      Validators.minLength(3),
      // this.censor('batman'),
    ], [
        this.asyncCensor
      ]],
    'extra': this.fb.group({
      'type': ['album'],
      'markets': this.fb.array([])
    })
  })

  extra = false
  markets = this.queryForm.get('extra.markets') as FormArray

  @Output() search = new EventEmitter<string>();

  constructor(private fb: FormBuilder) {
    (window as any).form = this.queryForm

    // this.search.subscribe()
  }

  addMarket() {
    this.markets.push(this.fb.group({
      'code': ['']
    }))
  }
  removeMarket(i: number) {
    this.markets.removeAt(i)
  }

  ngOnInit(): void {
    // https://rxmarbles.com/#of
    // https://rxjs.dev/operator-decision-tree

    const field = this.queryForm.get('query')!;

    const validChanges: Observable<'VALID' | 'INVALID' | 'PENDING'> = field.statusChanges
    const searchChanges: Observable<string> = field.valueChanges

    validChanges.pipe(
      withLatestFrom(searchChanges),
      debounceTime(300),
      filter(([status/* ,value */]) => status === 'VALID'),
      map(([ /* status */, value]) => value),
      distinctUntilChanged(),
    )
      .subscribe(this.search)
  }

  submit() {
    if (this.queryForm.invalid) { return }

    this.search.emit(this.queryForm.get('query')!.value)
  }

}
