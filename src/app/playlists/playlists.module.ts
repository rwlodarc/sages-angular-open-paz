import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './components/playlist-editor/playlist-editor.component';
import { SharedModule } from '../shared/shared.module';
import { PlaylistsNestedViewComponent } from './containers/playlists-nested-view/playlists-nested-view.component';
import { RoutedPlaylistsListComponent } from './components/routed-playlists-list/routed-playlists-list.component';
import { RoutedPlaylistDetailsComponent } from './components/routed-playlist-details/routed-playlist-details.component';
import { RoutedPlaylistEditorComponent } from './components/routed-playlist-editor/routed-playlist-editor.component';



@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent,
    PlaylistsNestedViewComponent,
    RoutedPlaylistsListComponent,
    RoutedPlaylistDetailsComponent,
    RoutedPlaylistEditorComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  exports: [
    PlaylistsViewComponent,
  ]
})
export class PlaylistsModule { }
