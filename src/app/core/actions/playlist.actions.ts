import { Playlist } from "src/app/playlists/model/Playlist";

export namespace Playlists {

    export class Add {
        static readonly type = '[Playlist] Add';
        constructor(public payload: Playlist) { }
    }

    export class Edit {
        static readonly type = '[Playlist] Edit';
        constructor(public payload: Playlist) { }
    }

    export class FetchAll {
        static readonly type = '[Playlist] Fetch All';
    }

    export class Delete {
        static readonly type = '[Playlist] Delete';
        constructor(public id: number) { }
    }

}
